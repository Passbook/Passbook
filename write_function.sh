#! /bin/bash
write(){
    zenity --info --title="Precautions" --text="Be carefull with what you are entering"
    read -p "Username:-" username
    read -p "Which type of Account?:-" account
    read -sp "Enter Password:-" password
    echo 
    read -sp "Enter password again:-" password1
    echo
    if [[ "$password" == "$password1" ]]
        then
            chmod 300 .safe
            echo "$username $account $password" >> .safe
            zenity --warning --text="Always use same password asked by openssl. Use password that you have used previous time. Otherwise you will loose your saved passwords"
            openssl enc -a --aes-256-cbc -in .safe -out .safe_safe
            chmod 000 .safe
            zenity --info --title="Done" --text="Your data is saved successfully"
    else 
        zenity --error --title="Mismatch" --text="Both the password does not match with each other \n Please run the command again"
        exit 0        
  fi
}
write
